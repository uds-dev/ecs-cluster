output "ecs_cluster_id" {
  value = "${aws_ecs_cluster.main.id}"
}
output "ecs_cluster_exec_role_arn" {
  value = "${aws_iam_role.ecs_service.arn}"
}
output "ecs_cluster_sg" {
  value = "${aws_security_group.app.id}"
}
