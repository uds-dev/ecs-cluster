# Terraform ECS Cluster module

A Terraform module containing ECS cluster with static autoscaling group and security group that
supports external docker registry authentication.

[Terraform version 0.12 compatible]

- `vpc_id` - VPC id to place the ECS cluster. (Required)
- `vpc_subnets_ids` - Subnets id's to place the ECS cluster instances. e.g. ['subnet-1a2b3c4d','subnet-1a2b3c4e','subnet-1a2b3c4f'](Required)
- `asg_size` - How many instances in the ASG group (Required)
- `internal_cidr_blocks` - A list of CIDR block to associate with the EC2 instances. e.g. ['0.0.0.0/0','10.0.0.0/16'](Required)
- `cluster_name` - ECS cluster name. (Required)
- `key_name` - SSH Key name (Required)
- `instance_type` - EC2 instance type. (Required)
- `instance_type` - EC2 instance type. (Required)
- `use_external_registry` - Authenticate against external Docker registry. (Optional)
- `ecs_engine_auth_type` - Type of authentication to use for docker registry (docker / dockercfg). (Optional. Required if `use_external_registry` is set to true)
- `ecs_engine_auth_data` - JSON data for dockercfg file. (Optional. Required if `use_external_registry` is set to true)

## Outputs

- `ecs_cluster_id` - The `id` of the ECS cluster.

## Usage example:

```
module "prometheus_ecs" {
  source                = "bitbucket.org/uds-dev/terraform-modules//ecs-cluster"
  cluster_name          = "${var.cluster_name}"
  vpc_id                = "${var.vpc_id}"
  vpc_subnets_ids       = "${var.vpc_subnets_ids}"
  internal_cidr_blocks  = "192.124.0.0/16"
  key_name              = "test-devops"
  instance_type         = "t2.small"
  asg_size              = "1"
  use_external_registry = "true"
  ecs_engine_auth_type  = "docker"
  ecs_engine_auth_data  = <<EOF
{
  "https://index.docker.io/v1/user": {
    "username": "my-username",
    "password": "my-password",
  }
}
EOF
}
```


## Setting up DVIKI with drone

```powershell
# Install choco - https://chocolatey.org/docs/installation#install-with-powershellexe
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install the following apps with choco
choco install git awscli tartool

# Run the script
.\install_drone_windows.ps1
```
