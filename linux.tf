resource "aws_autoscaling_group" "linux" {
  name                 = aws_launch_configuration.linux.name
  vpc_zone_identifier  = var.vpc_subnets_ids
  min_size             = var.asg_size_linux
  max_size             = "${var.asg_size_linux * 2}"
  desired_capacity     = var.asg_size_linux
  launch_configuration = aws_launch_configuration.linux.name

  tag {
    key                 = "Name"
    value               = "${var.cluster_name}-ecs-linux-host"
    propagate_at_launch = true
  }

  tag {
    key                 = "Grafana"
    value               = "true"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_launch_configuration" "linux" {
  security_groups             = [aws_security_group.app.id]
  key_name                    = var.key_name
  image_id                    = var.linux_ami_id  # data.aws_ami.stable_ecs_ami_linux.id
  instance_type               = var.instance_type_linux
  iam_instance_profile        = aws_iam_instance_profile.app.name
  associate_public_ip_address = false
  user_data                   = <<EOF
#!/bin/bash
echo -e "$(crontab -l)\n30 23 28 * * rm -f /tmp/cron.log >/dev/null " | crontab -
echo -e "$(crontab -l)\n0 0 * * * docker system prune --volumes -f >> /tmp/cron.log " | crontab -
echo -e "$(crontab -l)\n0 0 */14 * * docker system prune --volumes --all -f >> /tmp/cron.log " | crontab -

#Support S3 mount
curl -L https://github.com/kahing/goofys/releases/latest/download/goofys -o /bin/goofys
chmod +x /bin/goofys
mkdir -p /opt/videos
echo "goofys#binahai-qa-hmalgo   /opt/videos        fuse     _netdev,allow_other,--file-mode=0666,--dir-mode=0777,--acl=bucket-owner-full-control    0       0" >> /etc/fstab
goofys  --acl bucket-owner-full-control binahai-qa-hmalgo   /opt/videos

#Install and Start CloudWatch Agent
yum update -y && yum install -y amazon-cloudwatch-agent
/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c default

echo ECS_CLUSTER='${aws_ecs_cluster.main.name}' > /etc/ecs/ecs.config
echo ECS_AVAILABLE_LOGGING_DRIVERS=[\"json-file\",\"awslogs\",\"syslog\",\"journald\",\"gelf\",\"fluentd\", \"splunk\"] >> /etc/ecs/ecs.config
service docker restart
start ecs

# Mount NAS
NAS_FULLPATH="${var.nas_fullpath}"
NAS_LOCAL_MOUNT_DIR="${var.nas_local_mount_dir}"
echo "NAS Full Path: $NAS_FULLPATH"
echo "NAS Local Mount Dir: $NAS_LOCAL_MOUNT_DIR"
if [[ -n "$NAS_FULLPATH" && -n "$NAS_LOCAL_MOUNT_DIR" ]]; then
  set -x
  mkdir -p $NAS_LOCAL_MOUNT_DIR
  mount -t nfs --verbose -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $NAS_FULLPATH $NAS_LOCAL_MOUNT_DIR
  echo "$NAS_FULLPATH   $NAS_LOCAL_MOUNT_DIR        nfs     _netdev,nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport    0       0" >> /etc/fstab
  echo "Reloading fstab to make sure that it works"
  mount -a -t nfs
  set +x
else
  echo "Did not set NAS_FULLPATH and NAS_LOCAL_MOUNT_DIR"
fi

EOF

  root_block_device {
    volume_type = "gp3"
    volume_size = var.volume_size_linux
  }

  lifecycle {
    create_before_destroy = true
  }
}


data "aws_ami" "stable_ecs_ami_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.*-x86_64-ebs"]

  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}
