variable "vpc_id" {
  description = "VPC id to place the ECS cluster."
}

variable "vpc_subnets_ids" {
  description = "Subnets id's to place the ECS cluster instances. e.g. ['subnet-1a2b3c4d','subnet-1a2b3c4e','subnet-1a2b3c4f']"
  type        = list
}

variable "asg_size_linux" {
  description = "How many Linux instances in the ASG group"
}

variable "asg_size_arm" {
  description = "How many ARM instances in the ASG group"
}

variable "asg_size_windows" {
  description = "How many Windows instances in the ASG group"
}

variable "vpc_cidr_blocks" {
  description = "A list of CIDR blocks to associate with the EC2 instances. e.g. ['0.0.0.0/0','10.0.0.0/16']"
  type        = list
}

variable "cluster_name" {
  description = "ECS cluster name."
}

variable "key_name" {
  description = "SSH Key name"
}

variable "instance_type_linux" {
  description = "Linux EC2 instance type."
}

variable "instance_type_arm" {
  description = "ARM EC2 instance type"
}

variable "instance_type_windows" {
  description = "Windows EC2 instance type."
}

variable "volume_size_linux" {
  description = "Linux EC2 instance volume size."
  type        = number
  default     = 200
}

variable "volume_size_arm" {
  description = "ARM EC2 instance volume size."
  type        = number
  default     = 80
}

variable "volume_size_windows" {
  description = "Windows EC2 instance volume size."
  type        = number
  default     = 800
}

variable "ecs_engine_auth_type" {
  description = "Type of authentication to use for docker registry (docker / dockercfg)."
  default     = "dockercfg"
}

variable "ecs_engine_auth_data" {
  description = "JSON data for dockercfg file."
  default     = ""
}

variable "nas_fullpath" {
  description = "NAS IP Address and path to mount"
  type = string
  default = ""
}

variable "nas_local_mount_dir" {
  description = "Path to mount NAS locally"
  type = string
  default = ""  
}

variable "windows_ami_id" {
  type = string
}

variable "linux_ami_id" {
  type = string
}

variable "windows_volume_throughput" {
  type = number
  default = 125
}

variable "windows_volume_iops" {
  type = number
  default = 3000
}

locals {
  windows_volume_throughput = var.windows_volume_throughput
  windows_volume_iops = var.windows_volume_iops
}