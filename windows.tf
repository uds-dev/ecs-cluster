resource "aws_autoscaling_group" "windows" {
  name                 = aws_launch_configuration.windows.name
  vpc_zone_identifier  = var.vpc_subnets_ids
  min_size             = var.asg_size_windows
  max_size             = "${var.asg_size_windows * 2}"
  desired_capacity     = var.asg_size_windows
  launch_configuration = aws_launch_configuration.windows.name

  tag {
    key                 = "Name"
    value               = "${var.cluster_name}-ecs-windows-host"
    propagate_at_launch = true
  }

  tag {
    key                 = "Grafana"
    value               = "true"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_launch_configuration" "windows" {
  security_groups             = [aws_security_group.app.id]
  key_name                    = var.key_name
  image_id                    = var.windows_ami_id
  instance_type               = var.instance_type_windows
  iam_instance_profile        = aws_iam_instance_profile.app.name
  associate_public_ip_address = false
  user_data                   = <<EOF
<powershell>
xcopy "c:\Program Files\Git" c:\git /E/H/Y/I/Q

Import-Module ECSTools
Initialize-ECSAgent -Cluster '${aws_ecs_cluster.main.name}' -EnableTaskIAMRole -LoggingDrivers '["json-file","awslogs"]'
$Sta = New-ScheduledTaskAction -Execute "C:\Program Files\Docker\docker.exe" -Argument "system prune --volumes -f"
$Stt = New-ScheduledTaskTrigger -Daily -At 12am
Register-ScheduledTask Delete-Docker-Volumes -Action $Sta -Trigger $Stt
$Sta = New-ScheduledTaskAction -Execute "C:\Program Files\Docker\docker.exe" -Argument "system prune --volumes --all -f"
$Stt = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Saturday -At 12am
Register-ScheduledTask Delete-Docker-Images -Action $Sta -Trigger $Stt


mkdir C:\source
mkdir C:\source\repos
mkdir C:\source\repos\binahteaminstaller

mkdir C:\source\code
mkdir c:\source\code\recorder
mkdir c:\source\code\devapp
mkdir c:\source\code\sampleapp

cd C:\source
Invoke-WebRequest -Uri "https://aka.ms/vs/15/release/vs_buildtools.exe" -OutFile "vs_buildtools.exe"
Start-Process -FilePath "c:\source\vs_buildtools.exe" -ArgumentList "--add Microsoft.VisualStudio.Workload.MSBuildTools --quiet" -PassThru

Invoke-WebRequest -Uri "https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311.exe" -OutFile "wix311.exe"
Start-Process -FilePath "c:\source\wix311.exe" -ArgumentList "/install /quiet /norestart"  -PassThru

Invoke-WebRequest -Uri "https://www.ssl.com/download/29773/" -OutFile "CodeSignTool-v1.2.0.zip"
Start-Process -FilePath unzip -ArgumentList ".\CodeSignTool-v1.2.0.zip" -PassThru

mkdir C:\Drone
cd C:\Drone
Invoke-WebRequest -Uri "https://github.com/drone-runners/drone-runner-exec/releases/latest/download/drone_runner_exec_windows_amd64.tar.gz" -OutFile "drone_runner.tar.gz"
tar -xzf drone_runner.tar.gz
mv drone-runner-exec drone-runner-exec.exe
rm drone_runner.tar.gz
mkdir drone-runner-exec
Add-Content -Path drone-runner-exec\config -Value 'DRONE_RPC_PROTO=http'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_RPC_HOST=drone.binahai.com'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_RPC_SECRET=713d473557a71d3a6e942952b6c683d1'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_LOG_FILE=C:\Drone\drone-runner-exec\log.txt'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_UI_USERNAME=root'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_UI_PASSWORD=root'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_RUNNER_LABELS=runner:exec-windows'
Add-Content -Path drone-runner-exec\config -Value 'DRONE_DEBUG=true'
./drone-runner-exec.exe service install
./drone-runner-exec.exe service start

Remove-Item alias:curl
refreshenv

curl -sL -o "cwagent.msi" "https://s3.amazonaws.com/amazoncloudwatch-agent/windows/amd64/latest/amazon-cloudwatch-agent.msi"
Start-Process C:\Windows\System32\msiexec.exe  -ArgumentList "/i cwagent.msi /quiet /qn" -wait
& "C:\Program Files\Amazon\AmazonCloudWatchAgent\amazon-cloudwatch-agent-ctl.ps1" -m ec2 -a fetch-config -c default
& "C:\Program Files\Amazon\AmazonCloudWatchAgent\amazon-cloudwatch-agent-ctl.ps1" -m ec2 -a start

curl -sL -o "SSMAgent_latest.exe" "https://amazon-ssm-eu-west-1.s3.eu-west-1.amazonaws.com/latest/windows_amd64/AmazonSSMAgentSetup.exe"
Start-Process -FilePath SSMAgent_latest.exe -ArgumentList "/S"
$s = Get-Service AmazonSSMAgent
$s.status
Start-Service AmazonSSMAgent
Restart-Service AmazonSSMAgent
$s = Get-Service AmazonSSMAgent
$s.status

</powershell>
<persist>true</persist>
EOF

  root_block_device {
    volume_type = "gp3"
    volume_size = var.volume_size_windows
    throughput = local.windows_volume_throughput
    iops = local.windows_volume_iops
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_ami" "stable_ecs_ami_windows" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-ECS_Optimized-*"]

  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}
