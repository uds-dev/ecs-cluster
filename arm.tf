resource "aws_autoscaling_group" "arm" {
  name                 = aws_launch_configuration.arm.name
  vpc_zone_identifier  = var.vpc_subnets_ids
  min_size             = local.asg_size_arm
  max_size             = "${local.asg_size_arm * 2}"
  desired_capacity     = local.asg_size_arm
  launch_configuration = aws_launch_configuration.arm.name

  tag {
    key                 = "Name"
    value               = "${var.cluster_name}-ecs-arm-host"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_launch_configuration" "arm" {
  security_groups             = [aws_security_group.app.id]
  key_name                    = var.key_name
  image_id                    = local.arm_aws_ami_id
  instance_type               = local.instance_type_arm
  iam_instance_profile        = aws_iam_instance_profile.app.name
  associate_public_ip_address = false
  user_data                   = <<EOF
#!/bin/bash
echo -e "$(crontab -l)\n30 23 28 * * rm -f /tmp/cron.log >/dev/null " | crontab -
echo -e "$(crontab -l)\n0 0 * * * docker system prune --volumes -f >> /tmp/cron.log " | crontab -
echo -e "$(crontab -l)\n0 0 */14 * * docker system prune --volumes --all -f >> /tmp/cron.log " | crontab -

#Support S3 mount
curl -L https://github.com/kahing/goofys/releases/latest/download/goofys -o /bin/goofys
chmod +x /bin/goofys
mkdir -p /opt/videos
echo "goofys#binahai-qa-hmalgo   /opt/videos        fuse     _netdev,allow_other,--file-mode=0666,--dir-mode=0777,--acl=bucket-owner-full-control    0       0" >> /etc/fstab
goofys  --acl bucket-owner-full-control binahai-qa-hmalgo   /opt/videos

#Install and Start CloudWatch Agent
yum update -y && yum install -y amazon-cloudwatch-agent
/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c default

echo ECS_CLUSTER='${aws_ecs_cluster.main.name}' > /etc/ecs/ecs.config
echo ECS_AVAILABLE_LOGGING_DRIVERS=[\"json-file\",\"awslogs\",\"syslog\",\"journald\",\"gelf\",\"fluentd\", \"splunk\"] >> /etc/ecs/ecs.config
service docker restart
start ecs

# Mount NAS
NAS_FULLPATH="${var.nas_fullpath}"
NAS_LOCAL_MOUNT_DIR="${var.nas_local_mount_dir}"
echo "NAS Full Path: $NAS_FULLPATH"
echo "NAS Local Mount Dir: $NAS_LOCAL_MOUNT_DIR"
if [[ -n "$NAS_FULLPATH" && -n "$NAS_LOCAL_MOUNT_DIR" ]]; then
  set -x
  mkdir -p $NAS_LOCAL_MOUNT_DIR
  mount -t nfs --verbose -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $NAS_FULLPATH $NAS_LOCAL_MOUNT_DIR
  echo "$NAS_FULLPATH   $NAS_LOCAL_MOUNT_DIR        nfs     _netdev,nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport    0       0" >> /etc/fstab
  echo "Reloading fstab to make sure that it works"
  mount -a -t nfs
  set +x
else
  echo "Did not set NAS_FULLPATH and NAS_LOCAL_MOUNT_DIR"
fi

EOF

  root_block_device {
    volume_type = "gp3"
    volume_size = local.volume_size_arm
  }

  lifecycle {
    create_before_destroy = true
  }
}

locals {
  arm_aws_ami_id = "ami-027c9711e675a23d1"
  instance_type_arm = var.instance_type_arm
  volume_size_arm = var.volume_size_arm
  asg_size_arm = var.asg_size_arm
}
