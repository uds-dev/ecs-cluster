resource "aws_ecs_cluster" "main" {
  name = "${var.cluster_name}-ecs-cluster"
}

data "aws_arn" "cluster" {
  arn = "${aws_ecs_cluster.main.arn}"
}

resource "aws_iam_role" "ecs_service" {
  name               = "${var.cluster_name}-ecs_role-${data.aws_arn.cluster.region}"
  path               = "/"
  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [{
    "Action": "sts:AssumeRole",
    "Effect": "Allow",
    "Principal": {
      "Service": "ec2.amazonaws.com"
    }
  }]
}
EOF
}


resource "aws_iam_role_policy_attachment" "ec2_instance_role_managed_policy1" {
  role       = aws_iam_role.ecs_service.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


resource "aws_iam_role_policy_attachment" "ec2_instance_role_managed_policy2" {
  role       = aws_iam_role.ecs_service.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}


resource "aws_iam_role_policy_attachment" "ec2_instance_role_managed_policy3" {
  role       = aws_iam_role.ecs_service.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}


resource "aws_iam_role_policy_attachment" "ec2_instance_role_managed_policy4" {
  role       = aws_iam_role.ecs_service.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


resource "aws_iam_role_policy" "ecs_service" {
  name = "${var.cluster_name}-ecs_policy"
  role = aws_iam_role.ecs_service.name

  policy = <<EOF
{
  "Statement": [{
    "Effect": "Allow",
    "Action": [
      "ecs:CreateCluster",
      "ecs:DeregisterContainerInstance",
      "ecs:DiscoverPollEndpoint",
      "ecs:Poll",
      "ecs:RegisterContainerInstance",
      "ecs:StartTelemetrySession",
      "ecs:Submit*",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetAuthorizationToken",
      "ecs:DeregisterTaskDefinition",
      "ecs:DescribeServices",
      "ecs:DescribeTaskDefinition",
      "ecs:DescribeTasks",
      "ecs:ListTasks",
      "ecs:ListTaskDefinitions",
      "ecs:RegisterTaskDefinition",
      "ecs:StartTask",
      "ecs:StopTask",
      "ecs:UpdateService",
      "cloudwatch:Get*",
      "cloudwatch:List*"
    ],
    "Resource": "*"
  }]
}
EOF
}

resource "aws_iam_role_policy" "binahai-qa-hmalgo" {
  name = "${var.cluster_name}-binahai-qa-hmalgo"
  role = aws_iam_role.ecs_service.name

  policy = <<EOF
{
  "Statement": [{
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:DeleteObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "arn:aws:s3:::binahai-qa-hmalgo",
        "arn:aws:s3:::binahai-qa-hmalgo/*"
      ]
  }]
}
EOF
}

resource "aws_iam_instance_profile" "app" {
  name = "${var.cluster_name}-ecs-instprofile"
  path = "/"
  role = aws_iam_role.ecs_service.name
}

resource "aws_security_group" "app" {
  description = "controls direct access to application instances"
  vpc_id      = var.vpc_id
  name        = "${var.cluster_name}-ecs-runners-sg"

  ingress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = var.vpc_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
